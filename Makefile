SOURCE=MadewithCreativeCommonsmostup-to-dateversion

# Versioning: I'm using the date on which I got the source document
# (2017-06-09), plus an incrementing local part, that *should*
# increase every time the generated Markdown is modified.
VERSION=20170609-2

# --top-level-diversion is not available in pandoc in Jessie
PANDOCOPTS = --top-level-division=part -t docbook -f markdown+inline_notes
PANDOCOPTS = -t docbook -f markdown+inline_notes

$(SOURCE).md: $(SOURCE).odt fixup.rb
	TEMP=`tempfile -p mwcc` && \
	pandoc -f odt $(SOURCE).odt -t markdown > $$TEMP && \
	ruby fixup.rb $$TEMP $(SOURCE).md && \
	rm $$TEMP

pot: po/mwcc.pot po/*/mwcc.po
po/mwcc.pot: $(SOURCE).md
	TEMP=`tempfile -p mwcc` && \
	po4a-gettextize -f text -m $(SOURCE).md -p $$TEMP -M utf-8 --package-name 'Made with Creative Commons' --package-version $(VERSION) && \
	echo '# MADE WITH CREATIVE COMMONS' > $@ && \
	echo '# Copyright (C) 2017 by Creative Commons.' >> $@ && \
	echo '# This file is published under a Creative Commons Attribution-ShareAlike license (CC BY-SA), version 4.0' >> $@ && \
	echo '# Authors: Paul Stacey and Sarah Hinchliff Pearson' >> $@ && \
	tail --lines=+5 $$TEMP >> $@ && \
	rm $$TEMP
	for LANG in po/*/mwcc.po; do \
	    echo -n $$LANG\  ; \
	    msgmerge $$LANG po/mwcc.pot -U ; \
	done


%.xml: %.md
	pandoc -o $@ $(PANDOCOPTS) MadewithCreativeCommonsmostup-to-dateversion.md

pdf: MadewithCreativeCommonsmostup-to-dateversion.xml
	dblatex -b xetex MadewithCreativeCommonsmostup-to-dateversion.xml

XMLLINTOPTS = --nonet --noout  --xinclude --postvalid
lint: MadewithCreativeCommonsmostup-to-dateversion.xml
	xmllint $(XMLLINTOPTS) $^

.SUFFIXES: .xml .md
